<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostDataController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/postData', [PostDataController::class, 'get']);
Route::post('/postData/store', [PostDataController::class, 'store']);

Route::post('/postData/update/presentAbility', [PostDataController::class, 'updatePresentAbility']);
Route::post('/postData/update/sortAbility', [PostDataController::class, 'updateSortAbility']);
Route::post('/postData/update/searchAbility', [PostDataController::class, 'updateSearchAbility']);
