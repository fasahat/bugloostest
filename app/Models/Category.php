<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    public function categoryMeta(){

        return $this->hasMany(CategoryMeta::class);

    }

    public function parent()
    {
        
        return $this->belongsTo(Category::class,'parent_id')->where('parent_id',null);

    }

    public function children()
    {
        
        return $this->hasMany(Category::class,'parent_id');

    }


    /**
     * category has one to many relationship with PostColumn
     *
     * @return void
     */
    public function postColumns(){

        return $this->hasMany(PostColumn::class);

    }
}
