<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryMeta extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    protected $table = "category_meta";

    public function category(){

        return $this->belongsTo(Category::class);

    }
}
