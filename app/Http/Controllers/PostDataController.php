<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostDataController extends Controller
{
    /**
     * @api {post} /postData Request PostData information
     * @apiName GetPostData
     * @apiGroup PostData
     *
     * @apiParam {Number} id PostData unique ID.
     * @apiParam {boolean} Presentable postData with present ability or not.
     *
     * @apiParamExample  {json} Request-Example:
     * {
     *     "id" : 2,
     *     "presentable" : true,
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *      "data": [
     *          {
     *              "id": 29,
     *              "parentId": null,
     *              "userId": null,
     *              "title": "childrenToy",
     *              "type": null,
     *              "sortable": false,
     *              "searchable": false,
     *              "presentable": true,
     *              "paging": 10,
     *              "child": [
     *                  {
     *                      "id": 30,
     *                      "parentId": 29,
     *                      "userId": null,
     *                      "title": "children",
     *                      "type": null,
     *                      "sortable": false,
     *                      "searchable": false,
     *                      "presentable": true,
     *                      "paging": 10,
     *                      "child": []
     *                  }
     *              ]
     *          }  
     *     ] 
     * }
     * 
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 400 Bad Request
     * [
     *    "The selected id is invalid."
     * ]
     */
    public function get(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "nullable|exists:categories,id",
            "presentable" => "nullable|boolean"
        ]);

        if($validator->fails()) {
            return jsonErrorResponse($validator->errors()->all());
        } else {

            $postData = new \App\Http\Objects\PostData();
            // $postData->setPresentable(1);
            $result = $postData->get();
            return jsonSuccessResponse(["data" => $result]);
        }

    }

    
    public function store(Request $request)
    {
        $defaultInput = [
            "title" => null,
            "parentId" => null
        ];

        $validator = Validator::make($request->all(), [
            "title" => "required|string",
            "parentId" => "nullable|exists:categories,id",
        ]);

        if($validator->fails()) {
            return jsonErrorResponse($validator->errors()->all());
        } else {
            $inputData = $request->all();
            $inputData = array_merge_recursive_distinct(
                $defaultInput,
                $inputData,
            );

            $postData = new \App\Http\Objects\PostData();
            $postData->setType(\App\Http\Objects\PostData::POST_DATA_TYPE);
            $postData->setTitle($inputData["title"]);
            $postData->setParentId($inputData["parentId"]);
            $postData->create();
        }

        return jsonSuccessResponse(["message" => "stored successfully"]);

    }


    
    public function updatePresentAbility(Request $request)
    {
        $errors = [];
        $validator = Validator::make($request->all(), [
            "id" => "required|exists:categories,id",
            "presentAbility" => "required|boolean"
        ]);

        if($validator->fails()) {
            $errors = $validator->errors()->all();
        } else {
            try {
                $postData = new \App\Http\Objects\PostData();
                $postData->setId($request->id);
                $postData->setPresentable($request->presentAbility);
                $postData->updatePresentAbility();

            } catch (\App\Exceptions\InvalidArgumentException $exception) {

                $errors = $exception->getMessage();

            } catch (\App\Exceptions\InvalidDataAccessException $exception){
                
                $errors = $exception->getMessage();

            }

        }

        if(!empty($errors)) {
            
            return jsonErrorResponse($errors);

        } else {
            
            return jsonSuccessResponse(["message" => "updated successfully"]);

        }
    }

    
    public function updateSortAbility(Request $request){
        $errors = [];
        $validator = Validator::make($request->all(), [
            "id" => "required|exists:categories,id",
            "sortAbility" => "required|boolean"
        ]);

        if($validator->fails()) {
            $errors = $validator->errors()->all();
        } else {
            try {
                $postData = new \App\Http\Objects\PostData();
                $postData->setId($request->id);
                $postData->setSortable($request->sortAbility);
                $postData->updateSortAbility();

            } catch (\App\Exceptions\InvalidArgumentException $exception) {
                
                $errors = $exception->getMessage();

            } catch (\App\Exceptions\InvalidDataAccessException $exception){
                
                $errors = $exception->getMessage();

            }

        }

        if(!empty($errors)) {
            
            return jsonErrorResponse($errors);

        } else {
            
            return jsonSuccessResponse(["message" => "updated successfully"]);

        }
    }


    
    public function updateSearchAbility(Request $request){
        $errors = [];
        $validator = Validator::make($request->all(), [
            "id" => "required|exists:categories,id",
            "searchAbility" => "required|boolean"
        ]);

        if($validator->fails()) {
            $errors = $validator->errors()->all();
        } else {
            try {
                $postData = new \App\Http\Objects\PostData();
                $postData->setId($request->id);
                $postData->setSortable($request->searchAbility);
                $postData->updateSortAbility();

            } catch (\App\Exceptions\InvalidArgumentException $exception) {
                
                $errors = $exception->getMessage();

            } catch (\App\Exceptions\InvalidDataAccessException $exception){
                
                $errors = $exception->getMessage();

            }

        }

        if(!empty($errors)) {
            
            return jsonErrorResponse($errors);

        } else {
            
            return jsonSuccessResponse(["message" => "updated successfully"]);

        }
    }
}
