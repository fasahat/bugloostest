<?php 

namespace App\Http\Objects;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

class PostData implements \JsonSerializable{

    /**
     * key name for sorting ability of post date stored in CategoryMeta
     */
    const SORTABLE_KEY_NAME = "sortable";
    /**
     * key name for searching ability of post date stored in CategoryMeta 
     */
    const SEARCHABLE_KEY_NAME = "searchable";
    /**
     * key name for presenting ability of post data stored in CategoryMeta
     */
    const PRESENTABLE_KEY_NAME = "presentable";

    const DEFAULT_PAGE_COUNT = 10;

    const PAGING_KEY_NAME = "paging";

    const POST_DATA_META_TYPE = "post-data-meta";

    const POST_DATA_TYPE = "post-data";

    /**
     * identifier of category table
     * 
     * @var int
     */
    private $id;

    /**
     * parent identifier of current object from category table
     * main category will has null parentId
     * @var int|null
     */
    private $parentId;

    /**
     * user identifier of whom create column
     *
     * @var int
     */
    private $userId;

    /**
     * title of post data
     *
     * @var string
     */
    private $title;

    /**
     * type of column stored in list column table like "list"
     *
     * @var string
     */
    private $type;

    /**
     * is column sortable or not
     *
     * @var bool
     */
    private $sortable = false;

    /**
     * is column searchable or not
     *
     * @var bool
     */
    private $searchable = false;

    /**
     * column can display or not
     *
     * @var bool
     */
    private $presentable = true;

    /**
     * pagination of main category
     *
     * @var int
     */
    private $paging = self::DEFAULT_PAGE_COUNT;

    /**
     * if PostData is parent, so this property
     * consist of it's child
     * 
     * @var array of self object
     */
    private $child = [];

    /**
     * Set it identifies that current column will be used for which
     *
     * @param  int  $id  it identifies that current column will be used for which
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set main category will has null parentId
     *
     * @param  int|null  $parentId  main category will has null parentId
     *
     * @return  self
     */ 
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Set user identifier of whom create column
     *
     * @param  int  $userId  user identifier of whom create column
     *
     * @return  self
     */ 
    public function setUserId(int $userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Set title of post data
     *
     * @param  string  $title  title of post data
     *
     * @return  self
     */ 
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Set type of column stored in list column table like "list"
     *
     * @param  string  $type  type of column stored in list column table like "list"
     *
     * @return  self
     */ 
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set is column sortable or not
     *
     * @param  bool  $sortable  is column sortable or not
     *
     * @return  self
     */ 
    public function setSortable(bool $sortable = false)
    {
        $this->sortable = filter_var($sortable, FILTER_VALIDATE_BOOLEAN);

        return $this;
    }

    /**
     * Set is column searchable or not
     *
     * @param  bool  $searchable  is column searchable or not
     *
     * @return  self
     */ 
    public function setSearchable(bool $searchable = false)
    {
        $this->searchable = filter_var($searchable, FILTER_VALIDATE_BOOLEAN);

        return $this;
    }

    /**
     * Set column can display or not
     *
     * @param  bool  $presentable  column can display or not
     *
     * @return  self
     */ 
    public function setPresentable(bool $presentable = true)
    {
        $this->presentable = filter_var($presentable, FILTER_VALIDATE_BOOLEAN);

        return $this;
    }

    /**
     * Set pagination of main category
     *
     * @param  int  $paging  pagination of main category
     *
     * @return  self
     */ 
    public function setPaging(int $paging)
    {
        $this->paging = $paging;

        return $this;
    }

    /**
     * Get it identifies that current column will be used for which
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get user identifier of whom create column
     *
     * @return  int
     */ 
    public function getUserId()
    {
        return $this->userId;
    }


    /**
     * Get title of post data
     *
     * @return  string
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get type of column stored in list column table like "list"
     *
     * @return  string
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get is column sortable or not
     *
     * @return  bool
     */ 
    public function getSortable()
    {
        return $this->sortable;
    }

    /**
     * Get is column searchable or not
     *
     * @return  bool
     */ 
    public function getSearchable()
    {
        return $this->searchable;
    }

    /**
     * Get column can display or not
     *
     * @return  bool
     */ 
    public function getPresentable()
    {
        return $this->presentable;
    }


    /**
     * Get main category will has null parentId
     *
     * @return  int|null
     */ 
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Get pagination of main category
     *
     * @return  int
     */ 
    public function getPaging()
    {
        return $this->paging;
    }


    /**
     * Get of self object
     *
     * @return  array
     */ 
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set of self object
     *
     * @param  array $child  of self object
     *
     * @return  self
     */ 
    public function setChild(array $child)
    {
        $this->child = array_merge($child, $this->child);

        return $this;
    }

    /**
     * store parent and child categories 
     * set $this->meta according to parentId value if is null or not
     * if $this->parentId is null so it stores a parent category
     * else it stores a child category
     * 
     * @throws \App\Exceptions\InvalidArgumentException when setting invalid property data for 
     * \App\Repositories\CategoryRepository
     * 
     * @var \App\Repositories\CategoryRepository $categoryRepository.
     * 
     * @return void
     * 
     */
    public function create(){

        $categoryRepository = new \App\Repositories\CategoryRepository();

        $categoryRepository->setParentId($this->parentId)
                        ->setTitle($this->title)
                        ->setType($this->type);

        if(is_null($this->parentId)) {
        
            $categoryRepository->setMeta([
                [
                    "key" => self::PAGING_KEY_NAME,
                    "value" => $this->paging,
                    "type" => self::POST_DATA_META_TYPE
                ],
            ]);
        } else {
            $categoryRepository->setMeta([
                [
                    "key" => self::SORTABLE_KEY_NAME,
                    "value" => $this->sortable,
                    "type" => self::POST_DATA_META_TYPE
                ],
                [
                    "key" => self::SEARCHABLE_KEY_NAME,
                    "value" => $this->searchable,
                    "type" => self::POST_DATA_META_TYPE
                ],
                [
                    "key" => self::PRESENTABLE_KEY_NAME,
                    "value" => $this->presentable,
                    "type" => self::POST_DATA_META_TYPE
                ],
            ]);
        }

        $categoryRepository->save();
    
    }  

    /**
     * Update sort meta of category 
     * which defines is post data column sortable or not
     * 
     * @throws \App\Exceptions\InvalidArgumentException when there is not any category with given meta
     * @throws \App\Exceptions\InvalidDataAccessException 
     * 
     * @return void
     */
    public function updateSortAbility(){
        $categoryRepository = new \App\Repositories\CategoryRepository();
        $categoryRepository->setId($this->id)
            ->setMeta([
                [
                    "key" => self::SORTABLE_KEY_NAME,
                    "value" => $this->sortable,
                    "type" => self::POST_DATA_META_TYPE
                ]
            ])
            ->updateMeta();
    }

    /**
     * Update search meta of category 
     * which defines is post data column searchable or not
     * 
     * @throws \App\Exceptions\InvalidArgumentException when there is not any category with given meta
     * @throws \App\Exceptions\InvalidDataAccessException 
     * 
     * @return void
     */
    public function updateSearchAbility(){
        $categoryRepository = new \App\Repositories\CategoryRepository();
        $categoryRepository->setId($this->id);
        $categoryRepository->setMeta([
            [
                "key" => self::SEARCHABLE_KEY_NAME,
                "value" => $this->searchable,
                "type" => self::POST_DATA_META_TYPE
            ]
        ]);

        $categoryRepository->updateMeta();
    }

    /**
     * Update present meta of category 
     * which defines is post data column presentable or not
     * 
     * @throws \App\Exceptions\InvalidArgumentException when there is not any category with given meta
     * @throws \App\Exceptions\InvalidDataAccessException 
     * @return void
     */
    public function updatePresentAbility(){
        $categoryRepository = new \App\Repositories\CategoryRepository();
        $categoryRepository->setId($this->id);
        $categoryRepository->setMeta([
            [
                "key" => self::PRESENTABLE_KEY_NAME,
                "value" => $this->presentable,
                "type" => self::POST_DATA_META_TYPE
            ]
        ]);

        $categoryRepository->updateMeta();
    }

    /**
     * Update paging meta of category 
     * which defines how many post with related categories 
     * must display in list view
     * 
     * @throws \App\Exceptions\InvalidArgumentException when there is not any category with given meta
     * @throws \App\Exceptions\InvalidDataAccessException 
     * 
     * @return void
     */
    public function updatePaging(){
        $categoryRepository = new \App\Repositories\CategoryRepository();
        $categoryRepository->setId($this->id);
        $categoryRepository->setMeta([
            [
                "key" => self::PAGING_KEY_NAME,
                "value" => $this->paging,
                "type" => self::POST_DATA_META_TYPE
            ]
        ]);

        $categoryRepository->updateMeta();
    }

    /**
     * get children of a parent category
     * or children of all parent categories
     * 
     * @param integer|null $postDataId
     * @return array of $this objects
     */
    public function get(?int $postDataId = null,?bool $presentable = null){
        $categoryRepository = new \App\Repositories\CategoryRepository();
        $categoryRepository->setId($postDataId)
            ->setType(self::POST_DATA_TYPE);

        if(!is_null($presentable)) {
            $categoryRepository->setMeta([
                [
                    "key" => self::PRESENTABLE_KEY_NAME,
                    "value" => $this->presentable,
                    "type" => self::POST_DATA_META_TYPE
                ]
            ]);
        }
            
        $postDataCollection = $categoryRepository->get();
        return $this->mapPostDataCollection($postDataCollection);
    }

    /**
     * map collection of models into self object
     *
     * @param Collection $postDataCollection
     * @return void
     */
    private function mapPostDataCollection($postDataCollection){
        $postData = [];
        foreach($postDataCollection as $postDataModel) {
            $postDataParentObject = new self;
            $postDataParentObject->setId($postDataModel->id);
            $postDataParentObject->setParentId($postDataModel->parent_id);

            $postDataParentObject->setTitle($postDataModel->title);

            $postDataModel->categoryMeta->has("key", self::SORTABLE_KEY_NAME) ??
                $postDataParentObject->setSortable($postDataModel->categoryMeta->where("key", self::SORTABLE_KEY_NAME)->first()->value);

            $postDataModel->categoryMeta->has("key", self::SEARCHABLE_KEY_NAME) ??
                $postDataParentObject->setSearchable($postDataModel->categoryMeta->where("key", self::SEARCHABLE_KEY_NAME)->first()->value);

            $postDataModel->categoryMeta->has("key", self::PRESENTABLE_KEY_NAME) ??
                $postDataParentObject->setSearchable($postDataModel->categoryMeta->where("key", self::PRESENTABLE_KEY_NAME)->first()->value);


            if($postDataModel->children->isNotEmpty()){
                
                foreach( $postDataModel->children as $children) {
                    $postDataObject = new self;
                    $postDataObject->setId($children->id);
                    $postDataObject->setParentId($children->parent_id);
                    $postDataObject->setTitle($children->title);
    
                    $children->categoryMeta->has("key", self::SORTABLE_KEY_NAME) ??
                        $postDataObject->setSortable($children->categoryMeta->where("key", self::SORTABLE_KEY_NAME)->first()->value);

                    $children->categoryMeta->has("key", self::SEARCHABLE_KEY_NAME) ??  
                        $postDataObject->setSearchable($children->categoryMeta->where("key", self::SEARCHABLE_KEY_NAME)->first()->value);

                    $children->categoryMeta->has("key", self::PRESENTABLE_KEY_NAME) ??
                        $postDataObject->setPresentable($children->categoryMeta->where("key", self::PRESENTABLE_KEY_NAME)->first()->value);
         
                    $postDataParentObject->setChild([$postDataObject]);
                }
            }

            array_push($postData, $postDataParentObject);
            
        }
        return $postData;
    }

    /**
     * this method helps when using json_encode for current object
     * to return private properties
     *
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

}