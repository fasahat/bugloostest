<?php 

/**
* Return a new JSON response from the application.
*
* @param string|array $data
* @param int $status
* @param array $headers
* @param int $options
* @return \Symfony\Component\HttpFoundation\Response 
* @static 
*/
function jsonSuccessResponse($data = array(), $status = 200, $headers = array(), $options = 0){
    return response()->json($data, $status, $headers, $options);
}

function jsonErrorResponse($messages = array(), $status = 400, $headers = array(), $options = 0){
    return response()->json($messages, $status, $headers, $options);
}