<?php

namespace App\Console\Commands\Test;

use App\Http\Objects\PostData;
use Illuminate\Console\Command;

class PostDataTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'postData:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $postData = new PostData();

        /*************** test create *******/
        // $postData->setType("post-data");
        // $postData->setTitle("children");
        // $postData->setParentId(29);
        // $postData->create();

        /*************** test update sort ability ***************/

        // $postData->setId(30);
        // $postData->setSortable(0);
        // $postData->updateSortAbility();

        /*************** test update search ability ***************/

        // $postData->setId(30);
        // $postData->setSearchable(1);
        // $postData->updateSearchAbility();

        /*************** test update present ability ***************/

        // $postData->setId(30);
        // $postData->setPresentable(1);
        // $postData->updatePresentAbility();

        /*************** test update paging ***************/

        $postData->setId(29);
        $postData->setPaging(5);
        $postData->updatePaging();

        return 0;
    }
}
