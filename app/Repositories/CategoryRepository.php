<?php 

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\DTO\CategoryDTO;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository {

    /**
     * identifier of categories
     *
     * @var int|null
     */
    private $id;

    /**
     * parent identifier of current object in categories table
     *
     * @var int|null
     */
    private $parentId;

    /**
     * name of category
     *
     * @var string
     */
    private $title;

    /**
     * type of category 
     *
     * @var string
     */
    private $type;

    /**
     * array of arrays
     * structure like [["key", "value", "type"]]
     * 
     * @var array
     */
    private $meta = [];

    /**
     * Get identifier of categories
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier of categories
     *
     * @param  int|null  $id  identifier of categories
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * Get parent identifier of current object in categories table
     *
     * @return  int|null
     */ 
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set parent identifier of current object in categories table
     *
     * @param  int|null  $parentId  parent identifier of current object in categories table
     *
     * @return  self
     */ 
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get name of category
     *
     * @return  string
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name of category
     *
     * @param  string  $title  name of category
     *
     * @return  self
     */ 
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get type of category
     *
     * @return  string
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type of category
     *
     * @param  string  $type  type of category
     *
     * @return  self
     */ 
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get structure like [["key", "value", "type"]]
     *
     * @return  array
     */ 
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set structure like [["key", "value", "type"]]
     *
     * @param  array  $meta  structure like [["key", "value", "type"]]
     *
     * @return  self
     */ 
    public function setMeta(array $meta)
    {

        foreach($meta as $item) {
            if(count($item) != 3){

                throw new \App\Exceptions\InvalidArgumentException(["invalid value for meta property of ".__CLASS__]);

            } 
        }
        
        $this->meta = $meta;

        return $this;
    }


    public function save(){
        if(is_null($this->parentId)) {

            $this->saveParentCategory();

        } else {

            $this->saveChildCategory();

        }
    }

    private function saveParentCategory(){
        $postCategory = \App\Models\Category::create([
            "title" => $this->title,
            "type"  => $this->type,
        ]);

        if($postCategory) {
            $postCategory->categoryMeta()->createMany($this->meta);
        }
    }

    private function saveChildCategory(){

        $parentPostCategory = \App\Models\Category::find($this->parentId);

        if($parentPostCategory) {

            $postCategory = \App\Models\Category::create([
                "title" => $this->title,
                "type"  => $this->type,
                "parent_id" => $this->parentId
            ]);

            if($postCategory) {
                $postCategory->categoryMeta()->createMany($this->meta);
            }
            
        }
    }

    /**
     * update category meta of CategoryMeta
     *
     * @var int $metaTargetIndex - specify which element of $this->meta property 
     * is needed for updating CategoryMeta
     * 
     * @throws \App\Exceptions\InvalidArgumentException when there is not any category with given meta
     * @return void
     */
    public function updateMeta(){

        $metaTargetIndex = 0;
        $categoryMeta = new \App\Models\CategoryMeta();

        // check if given category has specific meta or not
        $category = $categoryMeta
            ->where([
                ["category_id", "=", $this->id],
                ["key", "=", $this->meta[$metaTargetIndex]["key"]],
                ["type", "=", $this->meta[$metaTargetIndex]["type"]]
            ])
            ->first();


        // if given category has meta so update it
        if($category) {
            $category->update([
                "value" => $this->meta[$metaTargetIndex]["value"]
            ]);
        } else {
            throw new \App\Exceptions\InvalidDataAccessException("invalid category access");
        }
    }

    public function get(){

        if(is_null($this->id)){
            $categories = \App\Models\Category::where("type", $this->type)->where("parent_id", null);
            
        } else {

            $categories = \App\Models\Category::where("type", $this->type)->where("parent_id", $this->id);

        }

        if(count($this->meta) == 1){
            $categories->with(['categoryMeta','children' => function ($query) {
                $query->whereHas("categoryMeta", function ($queryMeta) {
                    $queryMeta->where("key", '=', $this->meta[0]["key"])->where("value", "=",$this->meta[0]["value"]);
                })->with("categoryMeta");
            
            }]);
        } else {
            $categories->with('categoryMeta','children.categoryMeta');
        }
        return $categories->get();
        
    }

}