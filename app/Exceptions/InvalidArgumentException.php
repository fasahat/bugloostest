<?php

namespace App\Exceptions;

use Exception;

class InvalidArgumentException extends Exception
{
    /**
     * array of error messages
     *
     * @var array of strings
     */
    protected $message;

    public function __construct(array $message){

        $this->message = $message;

    }
}
